#!/bin/bash

USER=$(whoami)

DIRECTORY=/media/$USER/

# IFS stands for "internal field separator". It is used by the shell to determine how to do word splitting, 
# i. e. how to recognize word boundaries.
# From: https://unix.stackexchange.com/questions/184863/what-is-the-meaning-of-ifs-n-in-bash-scripting
IFS=$'\n'
# List all directories, remove trailing '/'
# Assign the output of the "ls -d DIRECTORY/ | sed -e 's-/$--'" command to an array named array
array=( $(ls -d $DIRECTORY* | sed -e 's-/$--') )
#If the length of the array is equal to 0 -> no usb connected because no directories.
if [ ${#array[@]} -eq 0 ]
then
  exit 1
fi
unset IFS

# Loop through all the elements in the array
for currentDir in ${array[@]}
do
	##Check if FC50-857B exists
	if [ -d "$currentDir" ]
	then
		# Control will enter here if $DIRECTORY exists.
		# Remove everything from the usb
		# rm -rf /media/pi/FC50-857B/*
	
		# Copy images, videos and audio files over to the USB
		cp -r /home/pi/Documents/Project/notaspy/static/images/ $currentDir
		cp -r /home/pi/Documents/Project/notaspy/static/videos/ $currentDir
		cp -r /home/pi/Documents/Project/notaspy/static/sound/ $currentDir
	
	else
		echo "$DIRECTORY not found"
	fi
done 