#!/bin/bash
# Returns used space on disk/partition in percent 

PARTITION=/dev/root

df -h | grep $PARTITION | tr -s " " | cut -d" " -f5