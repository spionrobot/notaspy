#!/bin/bash
# Removes all images, video and sound taken from the robot. This can be used to make space on the robot if there are many files/data on robot 

DIR=/home/pi/Documents/Project/notaspy/static

# Remove all images stored on the robot
rm $DIR/images/*
if [ $? -ne 0 ]
then
  exit 1 # Status code 1 indicates failure
fi

# Remove all videos stored on the robot
rm $DIR/videos/*
if [ $? -ne 0 ]
then
  exit 1 # Status code 1 indicates failure
fi

# Remove all sound-recordings stored on the robot
rm $DIR/sound/*
if [ $? -ne 0 ]
then
  exit 1 # Status code 1 indicates failure
fi

exit 0