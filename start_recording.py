# PyAudio provides Python bindings for PortAudio, the cross-platform audio I/O library.
# With PyAudio, you can easily use Python to play and record audio on a variety of platforms
# From: https://people.csail.mit.edu/hubert/pyaudio/docs/
import pyaudio

# The wave module provides a convenient interface to the WAV sound format.
# It does not support compression/decompression, but it does support mono/stereo.
# From: https://docs.python.org/3/library/wave.html
import wave

# Import time module to handle time-related tasks.
# Only import the sleep function from the time module
# The sleep() function suspends (delays) execution of the current thread for the given number of seconds.
# From https://www.programiz.com/python-programming/time
import time

class AudioRecording:
    form_1 = pyaudio.paInt16 # 16-bit resolution
    numberOfChannels = 1 # 1 channel
    samplingsRate = 44100 # 44.1kHz sampling rate
    chunk = 4096 # 2^12 samples for buffer
    record_secs = 9999 # seconds to record
    device_index = 2 # device index found by p.get_device_info_by_index(ii)
    directory_path_sound = '/home/pi/Documents/Project/notaspy/static/sound/' # directory path where to store the wav file.
    flag_record = False
    filename = ".wav"
    audio = 0
    stream = 0
    frameData = []

    # Returns a string to name the wav file
    def getNameWavFile(self, logger):
        # Get the current date and time
        localtime = time.asctime(time.localtime(time.time()))
        # The path and at the same time the name of the file.
        wav_name = localtime + ".wav"
        wav_name = wav_name.replace(" ", "") # Remove spaces from the name
        wav_name = wav_name.replace(":", "")  # Remove : from the name
        wav_name = wav_name.replace("-", "")  # Remove - from the name
        
        wav_name = self.directory_path_sound + wav_name


        logger.info("getNameWavFile returns: " + wav_name)
        return wav_name

    def Record(self, logger, event):
        logger.info("Record() inside start_recording.py was called")
        
        # Get the name of the file
        self.filename = self.getNameWavFile(logger)

        # create pyaudio instantiation
        self.audio = pyaudio.PyAudio()

        # create pyaudio stream
        self.stream = self.audio.open(format=self.form_1, rate=self.samplingsRate, channels=self.numberOfChannels,
                            input_device_index=self.device_index,
                            input=True, frames_per_buffer=self.chunk)

        frames = []

        while not event.wait(0):
            data = self.stream.read(self.chunk)
            frames.append(data)

        # Finished recording
        self.frameData = frames
        logger.info("Recording Stopped inside start_recording.py")
        self.StopRecording(logger)
        return

    def StartRecording(self, logger, event):
        logger.info("StartRecording inside start_recording.py has been called")
        self.flag_record = True
        self.Record(logger, event)
        return

    def StopRecording(self, logger):
        logger.info("StopRecording(self)")

        #If flag_record was already
        if(self.flag_record == False):
            return

        self.flag_record = False

        # stop the stream, close it, and terminate the pyaudio instantiation
        self.stream.stop_stream()
        self.stream.close()
        self.audio.terminate()

        # save the audio frames as .wav file
        wavefile = wave.open(self.filename, 'wb')
        wavefile.setnchannels(self.numberOfChannels)
        wavefile.setsampwidth(self.audio.get_sample_size(self.form_1))
        wavefile.setframerate(self.samplingsRate)
        wavefile.writeframes(b''.join(self.frameData))
        wavefile.close()
        return
