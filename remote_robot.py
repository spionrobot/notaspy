# Signal processing --> Python signal module is required for almost all the basic signal handling operations in python.
# A signal is like a notification of an event.
# When an event occurs in the system, a signal is generated to notify other programs about the event.
# For example, if you press Ctrl + c, a signal called SIGINT is generated and any program
# can actually be aware of that incident by simply reading that signal.
# Signals are identified by integers.
# From https://www.journaldev.com/16039/python-signal-processing
import signal

# The sys module provides information about constants, functions and methods of the Python interpreter
# The module sys informs e.g. about the maximal recursion depth (sys.getrecursionlimit() )
# and provides the possibility to change (sys.setrecursionlimit())
# The current version number of Python can be accessed as well
# From: https://www.python-course.eu/sys_module.php
import sys

# Python provides a logging system as a part of its standard library, so you can quickly add logging to your application.
# With the logging module imported, you can use something called a “logger” to log messages that you want to see.
# By default, there are 5 standard levels indicating the severity of events.
# Each has a corresponding method that can be used to log events at that level of severity.
# The defined levels, in order of increasing severity, are the following:
#    DEBUG
#    INFO
#    WARNING
#    ERROR
#    CRITICAL
# From https://realpython.com/python-logging/
import logging

# Import time module to handle time-related tasks.
# Only import the sleep function from the time module
# The sleep() function suspends (delays) execution of the current thread for the given number of seconds.
# From https://www.programiz.com/python-programming/time
import time
from time import sleep

import os

# When you do assert you're telling the program to test that condition,
# and immediately trigger an error if the condition is false.
# Assertions can include an optional message, and you can disable them when running the interpreter.
# From: https://stackoverflow.com/questions/5142418/what-is-the-use-of-assert-in-python
# sys.version --> Version number of the Python interpreter
# sys.version_info --> Similar information than sys.version, but the output is a
# tuple containing the five components of the version number: major, minor, micro, release-level, and serial.
# The values of this tuple are integers except the value for the release level,
# which is one of the following: 'alpha', 'beta', 'candidate', or 'final'.
# From: https://www.python-course.eu/sys_module.php
# Check what tuples in python are like: https://www.tutorialspoint.com/python3/python_tuples.htm
# Check if script is ran with Python3.
# Take the first value in the tuple and check if it is equal to the (3,) tuple. This way we check if this script
# is run with Python3
assert sys.version_info[0:1] == (3,)

#**********************************************************************************************************************
#**************************************Imports needed for web server***************************************************
#**********************************************************************************************************************

# Flask is a lightweight Python framework for web applications that provides the basics for URL routing and page rendering.
# Flask is a web framework. This means flask provides you with tools, libraries and technologies that allow you to
# build a web application. This web application can be some web pages, a blog, a wiki or go as big as a web-based
# calendar application or a commercial website. From: https://pymbook.readthedocs.io/en/latest/flask.html
# Python flask is an API that helps us to build web based application in python.
# From: https://www.journaldev.com/15524/python-flask-tutorial
# Import the:
#   Flask: Python class in flask module. The flask object implements a WSGI application and acts as the central object.
#                It is passed the name of the module or package of the application.
#                Once it is created it will act as a central registry for the view functions,
#                the URL rules, template configuration and much more.
#                From: https://flask.palletsprojects.com/en/1.1.x/api/?highlight=flask#flask.Flask
#   jsonify:
#                Python function in flask module
#                From: https://flask.palletsprojects.com/en/1.1.x/api/?highlight=jsonify#flask.json.jsonifya
#                From: https://codehandbook.org/working-with-json-in-python-flask/
#                From: https://codehandbook.org/create-json-using-python-flask/
#   render_template:
#                Python function in flask module. We use the render_template() method to render a html site template.
#                Flask makes uses of the Jinja2 template engine.
#                If you don't know what is a template language, it's simply HTML with variables and other
#                programming constructs like conditional statement and for loops etc.
#                This allows you to so some logic in the template itself.
#                The template language is then rendered into plain HTML before being sent to the client.
#                From: https://www.techiediaries.com/flask-tutorial-templates/
#   request:
#                Python attribute in flask module. - To access incoming request data, you can use the global request object.
#                Flask parses incoming request data for you and gives you access to it through that global object.
#                Internally Flask makes sure that you always get the correct data for the active thread if you are
#                in a multithreaded environment. The request object is an instance of a Request subclass and provides
#                all of the attributes Werkzeug defines. From: https://flask.palletsprojects.com/en/1.1.x/api/?highlight=request#flask.request
#                - To get the raw data, use request.data. This only works if it couldn't be parsed as form data,
#                otherwise it will be empty and request.form will have the parsed data. From: https://stackoverflow.com/questions/10434599/get-the-data-received-in-a-flask-request
#                For URL query parameters, use request.args., For posted form input, use request.form.
#                For JSON posted with content type application/json, use request.get_json(). From: https://stackoverflow.com/questions/10434599/get-the-data-received-in-a-flask-request
#   Response:
#                Python class in flask module. The response object that is used by default in Flask.
#                Works like the response object from Werkzeug but is set to have an HTML mimetype by default.
#                Quite often you don’t have to create this object yourself because make_response() will take care of that for you.
#                From: https://flask.palletsprojects.com/en/1.1.x/api/?highlight=request#flask.request
#   send_from_directory:
#                Python function in flask module. Send a file from a given directory with send_file().
#                This is a secure way to quickly expose static files from an upload folder or something similar.
#                From: https://flask.palletsprojects.com/en/1.1.x/api/?highlight=send_from_directory#flask.send_from_directory
#   url_for:
#               Python function in flask module. Generates a URL to the given endpoint with the method provided.
#               Variable arguments that are unknown to the target endpoint are appended to the generated URL as
#               query arguments. If the value of a query argument is None, the whole pair is skipped.
#               In case blueprints are active you can shortcut references to the same blueprint by prefixing the
#               local endpoint with a dot (.). From: https://flask.palletsprojects.com/en/1.1.x/api/?highlight=url_for#flask.url_for
from flask import Flask, jsonify, render_template, request, Response, send_from_directory, url_for

# Werkzeug is a comprehensive WSGI web application library. WSGI is the Web Server Gateway Interface.
# It is a specification that describes how a web server communicates with web applications, and how web applications
# can be chained together to process one request. It began as a simple collection of various utilities for
# WSGI applications and has become one of the most advanced WSGI utility libraries.
# From: https://pypi.org/project/Werkzeug/
# werkzeug.serving is a python module. There are many ways to serve a WSGI application.
# While you’re developing it, you usually don’t want to have a full-blown webserver like Apache up and running,
# but instead a simple standalone one. Because of that Werkzeug comes with a builtin development server.
# From: https://werkzeug.palletsprojects.com/en/0.16.x/serving/?highlight=werkzeug%20serving#module-werkzeug.serving
#   make_server:
#                Create a new server instance that is either threaded, or forks or just processes one request after another.
#                From: https://github.com/pallets/werkzeug/blob/master/src/werkzeug/serving.py
#
from werkzeug.serving import make_server

# gopigo3 module is meant for more advanced users. From: https://gopigo3.readthedocs.io/en/master/api-basic/structure.html
#   FirmwareVersionError:
#                If the GoPiGo3 firmware needs to be updated. It also debugs a message in the terminal.
#                From: https://gopigo3.readthedocs.io/en/master/api-basic/easygopigo3.html?highlight=firmwareversionerror
from gopigo3 import FirmwareVersionError

#   EasyGoPiGo3:
#                Python class in easygopigo3 module. This class is used for controlling a GoPiGo3 robot.
#                With this class you can do the following things with your GoPiGo3:
#                   - Drive your robot in any number of directions.
#                   - Have precise control over the direction of the robot.
#                   - Set the speed of the robot.
#                   - Turn on or off the blinker LEDs.
#                   - Control the GoPiGo3’ Dex’s eyes, color and so on …
#                Without a battery pack connected to the GoPiGo3, the robot won’t move.
#                From: https://gopigo3.readthedocs.io/en/master/api-basic/easygopigo3.html?highlight=firmwareversionerror
from easygopigo3 import EasyGoPiGo3


#**********************************************************************************************************************
#****************************************Imports needed for stream server**********************************************
#**********************************************************************************************************************

# Python io module allows us to manage the file-related input and output operations.
# The advantage of using the IO module is that the classes and functions available allows us to
# extend the functionality to enable writing to the Unicode data.
# From: https://www.journaldev.com/19178/python-io-bytesio-stringio
import io

# The Python picamera module/library allows you to control your Camera Module and create amazing projects.
# From: https://projects.raspberrypi.org/en/projects/getting-started-with-picamera/5
# This package provides a pure Python interface to the Raspberry Pi camera module for Python 2.7 (or above) or Python 3.2 (or above).
# From: https://picamera.readthedocs.io/en/release-1.13/
import picamera

# The SocketServer module simplifies the task of writing network servers.
# There are four basic concrete server classes:
#           TCPServer: This uses the Internet TCP protocol, which provides for continuous streams of data between the client and server.
#           UDPServer: This uses datagrams, which are discrete packets of information that may arrive out of order or be lost while in transit.
#           UnixStreamServer and UnixDatagramServer: These more infrequently used classes are similar to the TCP and
#                                                    UDP classes, but use Unix domain sockets; they’re not available
#                                                    on non-Unix platforms.
# From: https://docs.python.org/2/library/socketserver.html
import socketserver

# threading: This module constructs higher-level threading interfaces on top of the lower level thread module.
# From: https://docs.python.org/2/library/threading.html
#   Condition:
#           Python class in threading module. From: https://docs.python.org/2/library/threading.html?highlight=threading%20condition#threading.Condition
#   Thread:
#           Python class in threading module.
#           From: https://docs.python.org/2/library/threading.html?highlight=threading%20thread#threading.Thread
#   Event:
#           Python class in threading module.
#           From: https://docs.python.org/2/library/threading.html?highlight=threading%20event#threading.Event
import threading
from threading import Condition, Thread, Event

# http is a package that collects several modules for working with the HyperText Transfer Protocol:
#    - http.client is a low-level HTTP protocol client; for high-level URL opening use urllib.request
#    - http.server contains basic HTTP server classes based on socketserver
#    - http.cookies has utilities for implementing state management with cookies
#    - http.cookiejar provides persistence of cookies
# http is also a module that defines a number of HTTP status codes and associated messages through the http.HTTPStatus enum:
# From: https://docs.python.org/3/library/http.html
#   server:
#           contains basic HTTP server classes based on socketserver
#           This module defines classes for implementing HTTP servers (Web servers).
#           http.server is not recommended for production. It only implements basic security checks.
#           From: https://docs.python.org/3/library/http.server.html#module-http.server
from http import server

import start_recording

# The subprocess module allows you to spawn new processes, connect to their input/output/error pipes,
# and obtain their return codes. This module intends to replace several older modules and functions:Information
# about how the subprocess module can be used to replace these modules and functions
# can be found in the following sections.
from subprocess import call
import subprocess

from re import sub

# Import the module required to get distance sensor to work
# From: https://readthedocs.org/projects/di-sensors/downloads/pdf/master/
from di_sensors.easy_distance_sensor import EasyDistanceSensor
#from di_sensors.easy_light_color_sensor import EasyLightColorSensor
from di_sensors.light_color_sensor import LightColorSensor

# Import random to be able to 'generate'  a random number between 0 and 1
import random

# This function should be called from the main thread before other threads are started
# Does basic configuration for the logging system
# By using the level parameter, you can set what level of log messages you want to record.
# All events at or above DEBUG level will now get logged
# enable all logging calls at or above that level to be logged
logging.basicConfig(level = logging.DEBUG)

# For triggering the shutdown procedure when a signal is detected
# Create a new object of type Event() class
# This is one of the simplest mechanisms for communication between threads: one thread signals an event and one or more other threads are waiting for it.
# An event object manages an internal flag that can be set to true with the set() method and reset to false with the clear() method. The wait() method blocks until the flag is true.
keyboard_trigger = Event()
def signal_handler(signal, frame):
    logging.info('Signal detected. Stopping threads.')
    # Set the internal flag to true. All threads waiting for it to become true are awakened.
    # Threads that call wait() once the flag is true will not block at all.
    keyboard_trigger.set()

#**********************************************************************************************************************
#*************************************************Web Server Stuff*****************************************************
#**********************************************************************************************************************

# Directory Path can change depending on where you install this file.  Non-standard installations
# may require you to change this directory. Inside static are, jquery, nipplejs, and style.css
# directory_path = '/home/pi/Dexter/GoPiGo3/Projects/RemoteCameraRobot/static'
directory_path = '/home/pi/Documents/Project/notaspy/static'
directory_path_images = '/home/pi/Documents/Project/notaspy/static/images/'
directory_path_videos = '/home/pi/Documents/Project/notaspy/static/videos/'
directory_path_sounds = '/home/pi/Documents/Project/notaspy/static/sound/'
video_name_recent = ""
video_name_time = ""

distance_sensor_reading = 0.0
lightcolor_sensor_reading = 0.0

# Getters and Setters
def get_video_name_recent():
    return video_name_recent

def set_video_name_recent(name):
    global video_name_recent
    video_name_recent = name

def get_video_name_time():
    return video_name_time

def set_video_name_time(name):
    global video_name_time
    video_name_time = name

def get_distance_sensor_reading():
    return distance_sensor_reading

def set_distance_sensor_reading(value):
    global distance_sensor_reading
    distance_sensor_reading = value

def get_lightcolor_sensor_reading():
    return lightcolor_sensor_reading 

def set_lightcolor_sensor_reading(value):
    global lightcolor_sensor_reading 
    lightcolor_sensor_reading = value

MAX_FORCE = 5.0
MIN_SPEED = 100 # Minimum speed of the robot
MAX_SPEED = 300 # Maximum speed of the robot

# 'Try' block tests the created gopigo3_robot object for errors

# 'Except' block lets us handle the error, we have 3 except blocks here, IOError, FirmwareVersionError, and Exception
#   - IOError, when an I/O operation fails e.g "file not found"
#   - FirmwareVersionError, defined from the EasyGoPiGo3 library, says that the GoPiGo firmware needs to be updated
#   - Exception, occurs when there is an error in the program, this can be vague however,
# so unless the exception relates to the errors stated before, we only log this as "unexpected error",
# which could be a number of things,
# we then log the errors and set them to level critical, indicating that the program itself
# may be unable to continue running.

# Pythons 'logging' class is a means of tracking events that happen when the software runs
# the developer adds logging calls to the code to indicate that certain events have occured,
# in these cases, messages that say what errors occured.
try:
    gopigo3_robot = EasyGoPiGo3()
except IOError:
    logging.critical('GoPiGo3 is not detected.')
    sys.exit(1)
except FirmwareVersionError:
    logging.critical('GoPiGo3 firmware needs to be updated')
    sys.exit(2)
except Exception:
    logging.critical("Unexpected error when initializing GoPiGo3 object")
    sys.exit(3)

# Create an instance of the Distance Sensor class. init_distance_sensor raises ImportError
# when the di_sensors module can't be found.
try:
    #distance_sensor = gopigo3_robot.init_distance_sensor()
    # # instantiate the distance object
    distance_sensor = EasyDistanceSensor()
except ImportError as e:
    logging.critical('Distance Sensor - ImportError' + str(e))
    sys.exit(4)
except Exception as e:
    logging.critical('Distance Sensor - Exception: ' + str(e))

# Create an instance of the EasyLightColorSensor class. init_distance_sensor raises ImportError
# when the di_sensors module can't be found.
try:
    #distance_sensor = gopigo3_robot.init_distance_sensor()
    # # instantiate the distance object
    lightcolor_sensor = LightColorSensor()
except ImportError:
    logging.critical('Light&Color Sensor - ImportError')
    sys.exit(4)
except Exception:
    logging.critical('Light&Color Sensor - Exception' )

# The ip address for the HTTP Server. Used to create the webserver object
# when HOST = 0.0.0.0 -> Make the server externally visible, this tells the os to listen on all public IPs
HOST = "0.0.0.0"
# The port for the HTTP server. Used to create the webserver object. (Note: stream server has another port, this port is used only for the http server (web server))
WEB_PORT = 5000

# Create a Flask instance. An instance of this class will be our WSGI application
# The idea of the first parameter is to give Flask an idea of what belongs to your application.
# This name is used to find resources on the filesystem, can be used by extensions to improve debugging information and a lot more.
# So it’s important what you provide there. If you are using a single module, __name__ is always the correct value.
# If you however are using a package, it’s usually recommended to hardcode the name of your package there.
# static_url_path – can be used to specify a different path for the static files on the web.
# Defaults to the name of the static_folder folder. static_folder defaults to the static folder in the root path of the application.
app = Flask(__name__, static_url_path='')

# Create an instance of the AudioRecording class inside the start_recording.py file.
audioObject = start_recording.AudioRecording()

audio_event = threading.Event()
audio_thread = threading.Thread(target=audioObject.StartRecording, args=(logging, audio_event))

selfDriving_event = threading.Event()
selfDriving_thread = threading.Thread()

def get_audio_event():
    return audio_event

def set_audio_event():
    global audio_event
    audio_event = threading.Event()

def set_audio_thread():
    global audio_thread
    audio_thread = threading.Thread(target=audioObject.StartRecording, args=(logging, get_audio_event()))
    return

def get_audio_thread():
    return audio_thread

def get_selfDriving_event():
    return selfDriving_event

def set_selfDriving_event():
    global selfDriving_event
    selfDriving_event = threading.Event()

def set_selfDriving_thread():
    global selfDriving_thread
    selfDriving_thread = threading.Thread(target=selfDriving_Thread_Target, args=(logging, get_selfDriving_event()))

def get_selfDriving_thread():
    return selfDriving_thread

# Create a new class called WebServerThread that inherits from threading.Thread class
class WebServerThread(Thread):
    '''
    Class to make the launch of the flask server non-blocking.
    Also adds shutdown functionality to it.
    Many classes like to create objects with instances customized to a specific initial state.
    Therefore a class may define a special method named __init__(). When a class defines an __init__() method,
    class instantiation automatically invokes __init__() for the newly-created class instance.
    '''
    def __init__(self, app, host, port):
        Thread.__init__(self)
        # Create a new server instance that is either threaded, or forks or just processes one request after another.
        # Creates a new instance variable called srv. make_server returns BaseWSGIServer object which is a """Simple single-threaded, single-process WSGI server."""
        self.srv = make_server(host, port, app) 

        # Creates a new instance variable called ctx. Create an AppContext.
        # The application context binds an application object implicitly to the current thread or greenlet.
        # The application context keeps track of the application-level data during a request, CLI command, or other activity.
        # Rather than referring to an app directly, you use the current_app proxy, which points to the application handling the current activity.
        # From: https://flask.palletsprojects.com/en/1.1.x/appcontext/
        self.ctx = app.app_context()

        # call the ctx.push() function. Manually push the context.
        self.ctx.push()

    # Override the run function from the threading.Thread class. No arguments/parameters
    def run(self):
        logging.info('Starting Flask server')
        # Handle requests until an explicit shutdown() request. Poll for shutdown every poll_interval seconds (default=0.5)
        # BaseWGSIServer -> HTTPServer -> socketserver.serve_forever()
        self.srv.serve_forever()

    # No arguments/parameters
    def shutdown(self):
        logging.info('Stopping Flask server')
        # Tell the serve_forever() loop to stop and wait until it does. BaseWGSIServer -> HTTPServer -> socketserver.shutdown()
        self.srv.shutdown()

def move_robot(state, angle_degrees, angle_dir, determined_speed):
    #Cap out the determined_speed so that is does not exceed MAX_SPEED
    if determined_speed > MAX_SPEED:
        determined_speed = MAX_SPEED

    if state == 'move':
        # for moving backward
        if angle_degrees >= 260 and angle_degrees <= 280:
            gopigo3_robot.set_speed(determined_speed)
            gopigo3_robot.backward()

        # for moving to the left or forward
        if angle_degrees > 90 and angle_degrees < 260:
            gopigo3_robot.set_motor_dps(gopigo3_robot.MOTOR_RIGHT, determined_speed)

            left_motor_percentage = abs((angle_degrees - 170) / 90)
            sign = -1 if angle_degrees >= 180 else 1

            gopigo3_robot.set_motor_dps(gopigo3_robot.MOTOR_LEFT, determined_speed * left_motor_percentage * sign)

        # for moving to the right (or forward)- upper half
        if angle_degrees < 90 and angle_degrees >= 0:
            gopigo3_robot.set_motor_dps(gopigo3_robot.MOTOR_LEFT, determined_speed)

            right_motor_percentage = angle_degrees / 90
            gopigo3_robot.set_motor_dps(gopigo3_robot.MOTOR_RIGHT, determined_speed * right_motor_percentage)
        # for moving to the right (or forward)- bottom half
        if angle_degrees <= 360 and angle_degrees > 280:
            gopigo3_robot.set_motor_dps(gopigo3_robot.MOTOR_LEFT, determined_speed)

            right_motor_percentage = (angle_degrees - 280) / 80 - 1
            gopigo3_robot.set_motor_dps(gopigo3_robot.MOTOR_RIGHT, determined_speed * right_motor_percentage)

    elif state == 'stop':
        gopigo3_robot.stop()
    else:
        app.logging.warning('unknown state sent')

# We use the route() decorator to tell Flask what URL should trigger our function.
# The function is given a name which is also used to generate URLs for that particular function.
# Web applications use different HTTP methods when accessing URLs. For example GET and POST:
#   Get:
#       The GET method means retrieve whatever information (in the form of an entity) is identified by the Request-URI.
#   Post:
#       The POST method is used to request that the origin server accept the entity enclosed in the request as a new subordinate of the resource identified by the Request-URI in the Request-Line.
# By default, a route only answers to GET requests. You can use the methods argument of the route() decorator to handle different HTTP methods.
# If GET is present, Flask automatically adds support for the HEAD method and handles HEAD requests according to the HTTP RFC. Likewise, OPTIONS is automatically implemented for you.
# This function directly controls the robot by querying requests from the HTTP POST method
@app.route("/robot", methods = ["POST"])
def robot_commands():

    # get the query
    args = request.args # Get all the arguments from the request
    state = args['state'] # Get the state argument from the request
    angle_degrees = int(float(args['angle_degrees'])) # Get the angle_degrees argument from the request
    angle_dir = args['angle_dir'] # Get the angle_dir argument from the request
    force = float(args['force']) # Get the force argument from the request
    determined_speed = MIN_SPEED + force * (MAX_SPEED - MIN_SPEED) / MAX_FORCE # Calculate the speed robot will drive at

    move_robot(state, angle_degrees, angle_dir, determined_speed)

    # Generate a HTTP response code 200 OK
    resp = Response()
    resp.mimetype = "application/json"
    resp.status = "OK"
    resp.status_code = 200

    return resp

def update_distance_sensor_reading():
    # Reads the distance in millimeters. Update the reading
    reading = distance_sensor.read_mm()
    set_distance_sensor_reading(reading)

@app.route("/distance", methods = ['GET'])
def get_distance_sensor():
    # Update the reading before sending to the client
    update_distance_sensor_reading()
    # Typecast int to a string. 
    return str(get_distance_sensor_reading())

def update_lightcolor_sensor_reading():
    # Read the sensor values.
    # Returns The RGBA values from the sensor. RGBA = Red, Green, Blue, Alpha (or Clear).
    # Return a tuple
    percent_reading = lightcolor_sensor.get_raw_colors()
    set_lightcolor_sensor_reading(percent_reading)

@app.route("/lightcolor", methods = ['GET'])
def get_lightcolor_sensor():
    # Update the reading before sending it to the client
    update_lightcolor_sensor_reading()
    # Typecast to string
    return str(get_lightcolor_sensor_reading())

@app.route("/static/image", methods = ['POST'])
def send_take_picture():
    # logging.info("send_take_picture() has been called")

    localtime = time.asctime(time.localtime(time.time()))  # Get the current date and time
    # The path and at the same time the name of the file/image.
    image_name = localtime + ".jpeg"
    image_name = image_name.replace(" ", "") # Remove spaces from the name
    image_name = image_name.replace(":", "")  # Remove : from the name
    image_name = image_name.replace("-", "")  # Remove - from the name
    image_name_wd = image_name # The name of the image without directory path at the start of the string

    # The final name of the image with directory path at the start of the name
    image_name = directory_path_images + image_name

    try:
        camera.capture(image_name, format='jpeg', resize=(1920, 1080), splitter_port=0) # Capture a 1920x1080 jpeg image
    except Exception as e:
        logging.warning("Failed to capture image: " + str(e))
    finally: 
        # send the image thats just been taken
        return send_from_directory(directory_path_images, image_name_wd, mimetype='image/jpeg', as_attachment=True)


@app.route("/static/video", methods = ["POST"])
def start_stop_video():
    # get the query
    args = request.args  # Get all the arguments from the request

    # A flag that when is true the camera will start recording
    # When false the camera will stop recording
    recording = args['recording']

    # logging.info("start_stop_video() has been called, recording: " + recording)
    
    if recording == 'True' or recording == 'true':
        set_video_name_time(time.asctime(time.localtime(time.time())))  # Get the current date and time

    # The path and at the same time the name of the file/video. old format: .h264.
    video_name = directory_path_videos + get_video_name_time() + "_Video.h264"
    video_name = video_name.replace(" ", "")  # Remove spaces from the name
    video_name = video_name.replace(":", "")  # Remove : from the name
    video_name = video_name.replace("-", "")  # Remove - from the name

    video_name_mp4 = get_video_name_time() + "_Video.mkv"
    video_name_mp4 = video_name_mp4.replace(" ", "")
    video_name_mp4 = video_name_mp4.replace(":", "")
    video_name_mp4 = video_name_mp4.replace("-", "")

    video_name_mp4_wd = video_name_mp4
    set_video_name_recent(video_name_mp4_wd)

    video_name_mp4 = directory_path_videos + video_name_mp4

    if recording == 'True' or recording == 'true':
        camera.start_recording(video_name, splitter_port=1, resize=(320, 240))
        return "Started recording. Filename: " + video_name

    elif recording == 'False' or recording == 'false':
        try:
            camera.stop_recording(splitter_port=1)
            # Convert vide_name.h264 to video_name.mp4 so that the video is recorded in mp4 format
            #os.system("MP4Box -add " + video_name + " " + video_name_mp4) # Convert .h264 to .mp4
            os.system("ffmpeg -r 30 -i " + video_name + " -vcodec copy " + video_name_mp4)
            os.system("rm " + video_name) # Remove the 'old' .h264 video file
        except Exception as e:
            logging.warning("Possibly tried to stop video that has not been started yet. e: " + str(e))
        
        return send_from_directory(directory_path_videos, get_video_name_recent(), as_attachment=True)
    else:
        logging.warning("Something went terribly wrong inside start_stop_video()")
        return "Could not start or stop recording"

@app.route("/selfDriving", methods = ["POST"])
def start_stop_selfDriving():
    # Toggle self-driving
    # get the query
    args = request.args  # Get all the arguments from the request

    # A flag that when is true the robot will drive by itself dodging obstacles on its way
    # When false the robot will stop to drive itself
    selfDriving = args['selfDriving']

    if(selfDriving == 'True'):
        set_selfDriving_event()
        set_selfDriving_thread()
        get_selfDriving_thread().start()
        logging.info("Started self-driving")
        return "Started self-driving"
    else:
        get_selfDriving_event().set()
        selfDriving_thread.join()
        logging.info("Stopped self-driving")
        return "Stopped self-driving"

    return "Something exploded inside start_stop_selfDriving()"
    
def selfDriving_Thread_Target(logger, event):
    # Start self-driving
    while (not event.is_set()):
        # Stop robot
        move_robot('stop', 0, 'forward', 0)

        # Update sensors readings
        # update_distance_sensor_reading()
        # update_lightcolor_sensor_reading()

        # Check if there is some obstacle infront of the robot. 60 -> 60mm -> 6cm
        if(get_distance_sensor_reading() < 60):
            # Steer to the right or to the left
            number = random.random()
            if(number > 0.5):
                #Go to the right
                move_robot('move', 360.0, 'right', 100)
            else:
                #Go to the left
                move_robot('move', 180.0, 'left', 100)
            
        # Go forward
        move_robot('move', 89.0, 'forward', 100)

        # Sleep for some time, this means that the robot will move forward while sleeping. 0.2 seconds
        sleep(0.2)

# Returns used space on disk/partition in percent
@app.route("/usedSpaceDisk", methods = ['GET'])
def usedSpaceDisk():
    # run the GetAvailableSpaceDisk.sh bash script
    result = subprocess.run(['/home/pi/Documents/Project/notaspy/GetAvailableSpaceDisk.sh'], stdout=subprocess.PIPE)
    # Get the output from the bash script. Decode the string in utf-8. Store it in output variable
    output = result.stdout.decode('utf-8')
    # re.sub -> Return the string obtained by replacing the leftmost non-overlapping occurrences of pattern in string by the replacement repl. 
    # Remove all non numeric characters from output string. Update output
    output = sub("[^0-9]", "", output)
    # Convert output to string and return
    return str(output)

@app.route("/removeFilesOnRobot", methods = ['GET'])
def removeFilesOnRobot():
    #logging.info("removeFilesOnRobot() has been called!")
    # run the RemoveFilesOnRobot.sh bash script
    returnCode = call('/home/pi/Documents/Project/notaspy/RemoveFilesOnRobot.sh')

    #logging.info("RemoveFilesOnRobot.sh returnCode: " + str(returnCode))
    return str(returnCode)

# We use the route() decorator to tell Flask what URL should trigger our function.
# Here we server the "main" page (templates/index.html) that's styled with the (static/static.css) file
# We are calling the function below page() with "index.html"
@app.route("/")
def index():
    return page("index.html")

# We use the route() decorator to tell Flask what URL should trigger our function.
@app.route("/<string:page_name>")
def page(page_name):
    # render_template(template_name_or_list, **context) ->  Renders a template from the template folder with the given context.
    # format() -> Return a formatted version of S, using substitutions from args and kwargs.
    #             The substitutions are identified by braces ('{' and '}').
    return render_template("{}".format(page_name))

# We use the route() decorator to tell Flask what URL should trigger our function.
@app.route("/static/<path:path>")
def send_static(path):
    # send_from_directory(directory, filename, **options) -> Send a file from a given directory with send_file().
    #                        This is a secure way to quickly expose static files from an upload folder or something similar.
    # directory_path is the path to the static folder that contains all the static files
    return send_from_directory(directory_path, path)

# Return all images on the robot
@app.route("/list_images")
def list_images():
    #List of all filenames
    files = []
    # Go through every file in directory_path_images
    for fileName in os.listdir(directory_path_images):
        #Get the path to the file. Join the path components
        filePath = os.path.join(directory_path_images, fileName)
        #Check if the file exists. Returns true if the path is an existing regular file.
        if os.path.isfile(filePath):
            files.append(fileName)  # Add the name of the file at the end of the list
    return jsonify(files) # Convert the list to json format

# Return all videos on the robot
@app.route("/list_videos")
def list_videos():
    #List of all filenames
    files = []
    # Go through every file in directory_path_videos
    for fileName in os.listdir(directory_path_videos):
        #Get the path to the file. Join the path components
        filePath = os.path.join(directory_path_videos, fileName)
        #Check if the file exists. Returns true if the path is an existing regular file.
        if os.path.isfile(filePath):
            files.append(fileName)  # Add the name of the file at the end of the list
    return jsonify(files) # Convert the list to json format

# Return all sounds on the robot
@app.route("/list_sounds")
def list_sounds():
    logging.info("list_sounds() has been called")
    #List of all filenames
    files = []
    # Go through every file in directory_path_sound
    for fileName in os.listdir(directory_path_sounds):
        #Get the path to the file. Join the path components
        filePath = os.path.join(directory_path_sounds, fileName)
        #Check if the file exists. Returns true if the path is an existing regular file.
        if os.path.isfile(filePath):
            files.append(fileName)  # Add the name of the file at the end of the list
    return jsonify(files) # Convert the list to json format

# Function for downloading image from the robot
@app.route("/static/images/<filename>")
def get_image(filename):
    return send_from_directory(directory_path_images, filename, as_attachment=True);

# Function for downloading video from the robot
@app.route("/static/videos/<filename>")
def get_video(filename):
    return send_from_directory(directory_path_videos, filename, as_attachment=True);

@app.route("/static/sound", methods = ["POST"])
def start_stop_audio():
    # get the query
    args = request.args  # Get all the arguments from the request

    # A flag that when is true the camera will start recording
    # When false the camera will stop recording
    recording = args['recording']

    #logging.info("start_stop_audio() has been called, recording: " + recording)

    if(recording == 'True'):
        #logging.info("Started recording audio")
        #audioObject.StartRecording(logging)
        set_audio_event()
        set_audio_thread()
        get_audio_thread().start()
        return "Started recording audio"
    else:
        #audioObject.StopRecording(logging)
        get_audio_event().set()
        audio_thread.join()
        #logging.info("Stopped recording audio")
        return "Stopped recording audio"

    return "Something exploded inside start_stop_video()"

@app.route("/move_files")
def MoveOverFilesToUSB():
    #logging.info("MoveOverFilesToUSB() has been called!")
    #returnCode = call("./USBFileTransfer.sh")
    returnCode = call('/home/pi/Documents/Project/notaspy/USBFileTransfer.sh')

    #logging.info("returnCode: " + str(returnCode))
    return str(returnCode)

#**********************************************************************************************************************
#*******************************************Video Streaming Stuff******************************************************
#**********************************************************************************************************************

# Class to which the video output is written to.
# The buffer of this class is then read by StreamingHandler continuously.
class StreamingOutput(object):
    def __init__(self):
        self.frame = None

        # class io.BytesIO([initial_bytes])
        # A stream implementation using an in-memory bytes buffer.
        # It inherits BufferedIOBase. The buffer is discarded when the close() method is called.
        self.buffer = io.BytesIO()

        # threading.Condition(lock=None) This class implements condition variable objects.
        #                                A condition variable allows one or more threads to wait until they are
        #                                notified by another thread.
        self.condition = Condition()

    def write(self, buf):
        if buf.startswith(b'\xff\xd8'):
            # New frame, copy the existing buffer's content and notify all
            # clients it's available
            # io.IOBase.BufferedIOBase.BytesIO.truncate(size=None) -> Resize the stream to the given size in bytes
            #                                                         (or the current position if size is not specified)
            #                                                         The current stream position isn’t changed.
            #                                                         This resizing can extend or reduce the current
            #                                                         file size. In case of extension, the contents of
            #                                                         the new file area depend on the platform
            #                                                        (on most systems, additional bytes are zero-filled)
            #                                                         The new file size is returned.
            self.buffer.truncate()
            with self.condition:
                # getvalue() returns bytes containting the entire contents of the buffer
                self.frame = self.buffer.getvalue()
                self.condition.notify_all() # wakeup all threads waiting for this condition
            #  seek(offset, whence=SEEK_SET):
            #       Change the stream position to the given byte offset. offset is interpreted relative to the
            #       position indicated by whence. The default value for whence is SEEK_SET. Values for whence are:
            #           SEEK_SET or 0 – start of the stream (the default); offset should be zero or positive
            #           SEEK_CUR or 1 – current stream position; offset may be negative
            #           SEEK_END or 2 – end of the stream; offset is usually negative
            #       Return the new absolute position.
            # change the stream position to the start of the stream
            self.buffer.seek(0)
        # io.IOBase.BufferedIOBase.write(b):
        #       Write the given bytes-like object, b, and return the number of bytes written
        #       (always equal to the length of b in bytes, since if the write fails an OSError will be raised).
        return self.buffer.write(buf)

# Implementing GET request for the video stream.
# Inherits from the http.server.BaseHTTPRequestHandler class
# BaseHTTPRequestHandler:
#        This class is used to handle the HTTP requests that arrive at the server.
#        By itself, it cannot respond to any actual HTTP requests;
#        it must be subclassed to handle each request method (e.g. GET or POST).
#        BaseHTTPRequestHandler provides a number of class and instance variables, and methods for use by subclasses.
class StreamingHandler(server.BaseHTTPRequestHandler):
    # do_GET -> The request is mapped to a local file by interpreting the request as a path relative to the current working directory.
    # self.path -> Contains the request path
    # send_response(code, message=None) -> Adds a response header to the headers buffer and logs the accepted request.
    #                                      The HTTP response line is written to the internal buffer,
    #                                      followed by Server and Date headers.
    # send_header(keyword, value) -> Adds the HTTP header to an internal buffer which will be written to the output
    #                                stream when either end_headers() or flush_headers() is invoked. keyword should
    #                                specify the header keyword, with value specifying its value. Note that, after the
    #                                send_header calls are done, end_headers() MUST BE called in order to complete the
    #                                operation.
    # end_headers() -> Adds a blank line (indicating the end of the HTTP headers in the response) to
    #                  the headers buffer and calls flush_headers().
    def do_GET(self):
        if self.path == '/stream.mjpg': # Check if the request path is /stream.mjpg
            self.send_response(200)
            self.send_header('Age', 0)
            self.send_header('Cache-Control', 'no-cache, private')
            self.send_header('Pragma', 'no-cache')
            self.send_header('Content-Type', 'multipart/x-mixed-replace; boundary=FRAME')
            self.end_headers()
            try:
                # Infinite loop
                while True:
                    #output is an instance of StreamingOutput() class
                    with output.condition:
                        output.condition.wait() # Wait until notified or until a timeout occurs.
                                                # If the calling thread has not acquired the lock when this method is
                                                # called, a RuntimeError is raised.
                        frame = output.frame # update the frame
                    # wfile -> Contains the output stream for writing a response back to the client.
                    #          Proper adherence to the HTTP protocol must be used when writing to this stream in order
                    #          to achieve successful interoperation with HTTP clients.
                    #          This is an io.BufferedIOBase stream.
                    # wfile.write():
                    #       Write the given bytes-like object, b, and return the number of bytes written
                    #       (always equal to the length of b in bytes, since if the write fails an OSError will be raised).
                    # Write the frame
                    self.wfile.write(b'--FRAME\r\n')
                    self.send_header('Content-Type', 'image/jpeg')
                    self.send_header('Content-Length', len(frame))
                    self.end_headers()
                    self.wfile.write(frame)
                    self.wfile.write(b'\r\n')
            except Exception as e:
                logging.warning(
                    'Removed streaming client %s: %s',
                    self.client_address, str(e))
        else:
            self.send_error(404) # Error 404 Not Found
            self.end_headers() # End the response header

# Define a new class called StreamingServer that inherits from socketserver.ThreadingMixIn and server.HTTPServer
# socketserver -> The socketserver module simplifies the task of writing network servers.
# socketserver.ThreadingMixIn -> is used to support asynchronous behaviour when processing requests
# http.server.HTTPServer(server_address, RequestHandlerClass) -> This class builds on the TCPServer class by storing
#                                                                the server address as instance
#                                                                variables named server_name and server_port.
#                                                                The server is accessible by the handler, typically
#                                                                through the handler’s server instance variable.
# socketserver.ThreadingMixIn.allow_reuse_address:
#       Whether the server will allow the reuse of an address.
#       This defaults to False, and can be set in subclasses to change the policy.
# socketserver.ThreadingMixIn.daemon_threads:
#       When inheriting from ThreadingMixIn for threaded connection behavior, you should explicitly declare how
#       you want your threads to behave on an abrupt shutdown. The ThreadingMixIn class defines an attribute
#       daemon_threads, which indicates whether or not the server should wait for thread termination.
#       You should set the flag explicitly if you would like threads to behave autonomously; the default is False,
#       meaning that Python will not exit until all threads created by ThreadingMixIn have exited.
class StreamingServer(socketserver.ThreadingMixIn, server.HTTPServer):
    allow_reuse_address = True
    daemon_threads = True

#**********************************************************************************************************************
#***********************************************Aggregating All Calls**************************************************
#**********************************************************************************************************************
#The "main" method
if __name__ == "__main__":
    # registering both types of signals
    # Assign signal_handler handler to signal SIGINT and signal SIGTERM
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    # firing up the video camera (pi camera)
    # picamera.PiCamera -> Provides a pure Python interface to the Raspberry Pi’s camera module.
    #                      Upon construction, this class initializes the camera. The camera_num parameter
    #                      (which defaults to 0) selects the camera module that the instance will represent.
    #                      Only the Raspberry Pi compute module currently supports more than one camera.
    camera = picamera.PiCamera(resolution='320x240', framerate=30)

    # Create an instance of StreamingOutput class. This is the target where the output from camera.start_recording
    # will be written to
    output = StreamingOutput()

    # start recording video from the camera, storing it in output
    # If output is a string, it will be treated as a filename for a new file which the video will be written to.
    # If output is not a string, but is an object with a write method, it is assumed to be a file-like object and the
    # video data is appended to it (the implementation only assumes the object has a write() method - no other methods
    # are required but flush will be called at the end of recording if it is present). If output is not a string, and
    # has no write method it is assumed to be a writeable object implementing the buffer protocol.
    # In this case, the video frames will be written sequentially to the underlying buffer (which must be large enough
    # to accept all frame data).
    # format=mjpeg -> Write an M-JPEG video stream
    camera.start_recording(output, format='mjpeg', splitter_port=2)

    #Write to the logger, info level, inform that picamera started recording
    logging.info("Started recording with picamera")

    #The port number to open the stream at (note: not the same as the webserver port)
    STREAM_PORT = 5001

    # Create a new instance of the StreamingServer class
    # (HOST, STREAM_PORT) -> server_address
    # StreamingHandler -> RequestHandlerClass
    stream = StreamingServer((HOST, STREAM_PORT), StreamingHandler)

    # starting the video streaming server
    # stream.server_forever -> Handle one request at a time until shutdown
    # streamserver is a thread where the target of the thread is stream.serve_forever function/method.
    streamserver = Thread(target = stream.serve_forever)
    streamserver.start() # start the thread (streamserver)
    logging.info("Started stream server for picamera")

    # starting the web server
    # app -> flask instance
    # HOST -> 0.0.0.0 in order to listen on all public IP's
    # WEB_PORT -> 5000
    webserver = WebServerThread(app, HOST, WEB_PORT)
    webserver.start() # start the thread (webserver)
    logging.info("Started Flask web server")

    # and run it indefinitely
    # When SIGINT or SIGTERM signals are detected the keyboard_trigger is set.
    # Loop unitil the trigger is NOT set
    # This loop will poll for keyboard_trigger every 0.5 seconds
    while not keyboard_trigger.is_set():
        # Suspend execution of the calling thread for the given number of seconds.
        # Make the main thread sleep for 0.5 seconds
        sleep(0.5)

    # until some keyboard event is detected
    logging.info("Keyboard event detected")

    # trigger shutdown procedure
    webserver.shutdown()
    camera.stop_recording()
    stream.shutdown()

    # and finalize shutting them down
    webserver.join()
    streamserver.join()
    logging.info("Stopped all threads")

    sys.exit(0)
